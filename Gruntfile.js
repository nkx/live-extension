/*jshint esnext: true, evil: true*/


/**
 *
 * @param {[[Type]]} grunt [[Description]]
 */
module.exports = function (grunt) {
  'use strict';
  require('load-grunt-tasks')(grunt);

  // Project configuration
  grunt.initConfig({
    // Metadata
    pkg: grunt.file.readJSON('package.json'),
    gsh: grunt.file.readJSON('src/metadata.json'),

    banner: '/*jshint <%= pkg.jshintoptions %>*/\n/*globals <%= pkg.jshintglobals %>*/\n'+
    '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
    '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
    '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
    '* Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %>;' +
    ' Licensed <%= pkg.license %> */\n',

    // Task configuration
    jshint: {
      files: ['Gruntfile.js', 'src/**.js'],
      options: {
        globals: {
          global:true, imports:true, log:true
        },
        esnext: true, evil: true
      }
    },
    clean: {
      dist: {
        src: ['dist','build'],
      },
    },
    concat: {
      options: {
        banner: '<%= banner %>',
        stripBanners: true
      },
      dist: {
        src: ['src/extension.js'],
        dest: 'dist/extension.js'
      }
    },
    shell: {
      options: {
        stderr: false
      },
      renameBuildedSrc: {
        command: 'mv ./build/src/* ./<%= gsh.uuid %>',
      },
      buildInfo: {
        command: 'ls build/** -d',
      },
      // rename build directory to dist
      makeDist: {
        command: 'mv build dist && echo "distribution done.\nall needed files in $(pwd)dist/"',
      }
    },
    watch: {

    },
    copy: {
      main: {
        files:[{
          // Copy conconated files
          expand: true,
          flatten: false,
          src: ['src/**/*.js','src/*.{json,css}'],
          filter: "isFile",
          dest: "build/",
          rename: function(dest, src) {
            return dest + src.replace(/^src\//,'');
          }
        }]
      },
    }
  });

  // These plugins provide necessary tasks
  /*
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  //grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');*/

  // Default task
  grunt.registerTask('default', ['clean','jshint', 'concat']);
  grunt.registerTask('build', ['clean', 'copy:main','shell:buildInfo']);
  grunt.registerTask('dist', ['build', 'shell:makeDist']);

  // Gnome-Shell specific tasks
  grunt.registerTask('gnome-shell-setup', 'install extension', function(){

  });
  grunt.registerTask('gnome-shell-install', 'install extension', function(){

  });

  //grunt.registerTask('install', ['copy:main','shell:renameBuildedSrc']);
};
