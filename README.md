#  [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]

> Enables (auto)reloa of specific gnome extensionsd


## Install

```sh
$ npm install --save gnome-shell-live-extension
```


## Usage

```js
var gnomeShellLiveExtension = require('gnome-shell-live-extension');

gnomeShellLiveExtension('Rainbow');
```


## License

MIT © [nk](k46.net)


[npm-image]: https://badge.fury.io/js/gnome-shell-live-extension.svg
[npm-url]: https://npmjs.org/package/gnome-shell-live-extension
[travis-image]: https://travis-ci.org/nkx/gnome-shell-live-extension.svg?branch=master
[travis-url]: https://travis-ci.org/nkx/gnome-shell-live-extension
[daviddm-image]: https://david-dm.org/nkx/gnome-shell-live-extension.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/nkx/gnome-shell-live-extension
