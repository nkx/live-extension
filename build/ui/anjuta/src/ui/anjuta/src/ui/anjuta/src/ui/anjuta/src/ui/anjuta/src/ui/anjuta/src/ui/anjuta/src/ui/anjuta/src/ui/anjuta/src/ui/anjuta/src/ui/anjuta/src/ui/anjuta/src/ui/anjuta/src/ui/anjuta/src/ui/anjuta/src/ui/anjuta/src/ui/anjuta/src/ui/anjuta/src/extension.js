/*jshint esnext: true, evil: true*/
/* globals global, imports, log, init */



const Lang = imports.lang;
//const Gettext = imports.gettext.domain("drop-down-terminal");
const St = imports.gi.St;
const Main = imports.ui.main;
const Tweener = imports.ui.tweener;

const Gio = imports.gi.Gio;
//const GLib = imports.gi.GLib;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;

const ExtensionUtils = imports.misc.extensionUtils;
//const ExtensionSystem = imports.ui.extensionSystem;
//const ExtensionState = ExtensionSystem.ExtensionState;

const REAL_SHORTCUT_SETTING_KEY = "real-shortcut";
/*const CURRENT_EXTENSION = {
  uuid: 'apache-tools@joedev.k46.net',
  path: "/home/joedev/Dokumente/Develop/Sources/Gnome-Shell-Extensions/apache-tools@joedev.k46.net"
};*/

const Extension = ExtensionUtils.extensions['live-extension-nk@k46.net'];
const ExtensionHandler = Extension.imports.extensionHandler;

/*const key_bindings = {
  'toggle-terminal': function() {

  }
};*/


//const ExtensionHandler = imports.extensionHandler

let text, button;

/*function _hideLog() {
  Main.uiGroup.remove_actor(text);
  text = null;
}*/
function _log(msg,opt) {
  if (!text) {
    text = new St.Label({ style_class: 'helloworld-label', text: msg });
    Main.uiGroup.add_actor(text);
  }


  text.opacity = 255;

  let monitor = Main.layoutManager.primaryMonitor;

  text.set_position(Math.floor(monitor.width / 2 - text.width / 2),
                    Math.floor(monitor.height / 2 - text.height / 2));

  var defopt = { opacity: 0, delay: 1, time: 1,
                transition: 'easeInOutQuad'};
  for(var key in opt) { defopt[key] = opt[key]; }
  Tweener.addTween(text, defopt);
}
function _getLiveExt() {
  /*let ext = {
    uuid: CURRENT_EXTENSION.uuid,
    path: CURRENT_EXTENSION.path
  };*/

  let ext = {
    uuid: 'dev-test@nk.k46.net',
    path: '/home/joedev/Dokumente/Develop/Workspace/dev-test/src'
  };
  ext.dir = Gio.File.new_for_path(ext.path);
  return ext;
}
function _reloadCurrentExtension() {
  let ext = _getLiveExt ();

  ExtensionHandler.toggle(ext);
  _log('reload '+ext.uuid);
}

// extension class
const LiveExtension = new Lang.Class({
  Name: "LiveExtension",
  _init: function() {
    button = new St.Bin({ style_class: 'panel-button', reactive: true, can_focus: true, x_fill: true, y_fill: false, track_hover: true });
    let icon = new St.Icon({ icon_name: 'view-refresh-symbolic', style_class: 'system-status-icon' });
    button.set_child(icon);

    try{
      //const ExtensionHandler = Extension.extensionHandler;
      button.connect('button-press-event', _reloadCurrentExtension);
    }catch(e){
      log("Error while reloading script."+e.toString(),{delay:3});
      //_log("Error while reloading script."+e.toString(),{delay:3});
    }
  },


  _unbindShortcut: function() {
    if (Main.wm.removeKeybinding) {// introduced in 3.7.2
      Main.wm.removeKeybinding(REAL_SHORTCUT_SETTING_KEY);
    } else {
      global.display.remove_keybinding(REAL_SHORTCUT_SETTING_KEY);
    }
  },
  _bindShortcut: function() {
    if (Main.wm.addKeybinding && Shell.ActionMode) { // introduced in 3.16
      Main.wm.addKeybinding(REAL_SHORTCUT_SETTING_KEY, this._settings, Meta.KeyBindingFlags.NONE,
                            Shell.ActionMode.NORMAL | Shell.ActionMode.MESSAGE_TRAY,
                            Lang.bind(this, this._toggle));
    } else if (Main.wm.addKeybinding && Shell.KeyBindingMode) { // introduced in 3.7.5
      Main.wm.addKeybinding(REAL_SHORTCUT_SETTING_KEY, this._settings, Meta.KeyBindingFlags.NONE,
                            Shell.KeyBindingMode.NORMAL | Shell.KeyBindingMode.MESSAGE_TRAY,
                            Lang.bind(this, this._toggle));
    } else if (Main.wm.addKeybinding && Main.KeybindingMode) { // introduced in 3.7.2
      Main.wm.addKeybinding(REAL_SHORTCUT_SETTING_KEY, this._settings, Meta.KeyBindingFlags.NONE,
                            Main.KeybindingMode.NORMAL | Main.KeybindingMode.MESSAGE_TRAY,
                            Lang.bind(this, this._toggle));
    } else {
      global.display.add_keybinding(REAL_SHORTCUT_SETTING_KEY, this._settings, Meta.KeyBindingFlags.NONE,
                                    Lang.bind(this, this._toggle));
    }

  },
  enable: function() {
    Main.wm.setCustomKeybindingHandler('panel-main-menu', Shell.KeyBindingMode.NORMAL | Shell.KeyBindingMode.OVERVIEW, function() {
      _reloadCurrentExtension();
    });
    Main.panel._rightBox.insert_child_at_index(button, 0);
  },
  disable: function() {
    Main.panel._rightBox.remove_child(button);
  }
});

/*jshint -W098 */
function init() {
  return new LiveExtension();
}
