/*jshint globals: true, esnext: true, evil: true, -W098*/
/* globals imports, print, log */

// -*- mode: js; js-indent-level: 4; indent-tabs-mode: nil -*-
//imports.searchPath.unshift("/home/joedev/Dokumente/Develop/Workspace/net.k46.gnome-shell-lib/src");

// Common utils for the extension system and the extension
// preferences tool

//const Lang                      = imports.lang;
//const Signals                   = imports.signals;

//const GLib                      = imports.gi.GLib;
//const Gio                       = imports.gi.Gio;
//const ShellJS                   = imports.gi.ShellJS;

const ExtensionUtils            = imports.misc.extensionUtils;
const ExtensionType             = ExtensionUtils.ExtensionType;
const ExtensionSystem           = imports.ui.extensionSystem;
const ExtensionState            = ExtensionSystem.ExtensionState;


//const Debug					    = imports.utils.debug.Debug;

let Debug = {
    log: function(msg) {
        print('[ExtLog] '+msg.toString());
    },
    inspect: function(obj)  {
        var str;
        for(var key in obj) {
            str += (key+":"+(obj[key].toString() || typeof(obj[key])));
            str += "\n";
        }
        if(str) {
          Debug.log(str);
        }
    }
};
let handledUuid;
/*
 *
ExtensionSystem.connect('extension-state-changed',function(event,ext){

    if(handledUuid && handledUuid == ext.uuid) {

        if(ext.state !== ExtensionState.UNINSTALLED) {
            Debug.log('Found '+ext.uuid);
            Debug.log('State '+ext.state);
            currentHandledExtension = ext;
        }
    }

    //currentHandledExtension = ext
});
*/

function _loadExt(uuid, dir) {
	return ExtensionUtils.createExtensionObject(uuid, dir, ExtensionType.PER_USER);
}
function _unloadExt(extension) {
	return ExtensionSystem.unloadExtension(extension);
}
/**
 * reload an extension
 * @param object extension
 */
function reload(extension) {
    handledUuid=extension.uuid;
    if(!handledUuid) {throw('uuid is missing!');}
    extension = (ExtensionUtils.extensions[handledUuid] || extension);
    ExtensionSystem.disableExtension(handledUuid);
    ExtensionSystem.reloadExtension(extension);
    ExtensionSystem.enableExtension(handledUuid);
}
/**
 * toggle activity of an extension
 * @param object extension
 */
function toggle(extension) {
    handledUuid=extension.uuid;
	if(!handledUuid) {throw('uuid is missing!');}
	extension = (ExtensionUtils.extensions[handledUuid] || extension);
    var state=extension.state;
	if (state === undefined) {
        Debug.log('Create Extension');
	    extension = ExtensionUtils.createExtensionObject(
            handledUuid, extension.dir, extension.type
        );
        state = extension.state;
	} else {
        ExtensionSystem.reloadExtension(extension);
        extension = ExtensionUtils.extensions[handledUuid];
    }
	if(!state || state === ExtensionState.OUT_OF_DATE) {
		Debug.log('initializing extension');
        ExtensionSystem.initExtension(handledUuid);
        state = extension.state;
	}

    //
	if (state === ExtensionState.ENABLED) {
        Debug.log('disable extension');
	    ExtensionSystem.disableExtension(handledUuid);
	} else if (state === ExtensionState.DISABLED) {
        Debug.log('enable extension');
		ExtensionSystem.enableExtension(handledUuid);
	}

}
