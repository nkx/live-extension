# README #


### What is this repository for? ###

Gnome-Shell Extension that enables (auto)reloading for specific Extensions.

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Run install.sh. That will copy the Extension to the gnome-shell Extension directory. 
After this you have to reload the shell (Alt+F2 r)

You should see an reload icon in the upper right corner of the top panel.

